#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <poll.h>
#include <sys/wait.h>
#include "sniffer_functions.h"

//-----------------------------------------------------------------------------------------------
//------------------------------------LIST FOR AVAILABLE WORKERS---------------------------------
//-----------------------------------------------------------------------------------------------


//CREATING LIST
workers CREATE_LIST_OF_AVAILABLE_WORKERS(){
        workers list=NULL;
        list=malloc(sizeof(worker_node));
        if(list==NULL) exit(20);
        list->number_of_available_workers=0;
        list->start=NULL;
        return list;
}

//CHECKING IF LIST IS EMPTY
int EMPTY_LIST(workers list){
        if(list->start==NULL) return 1;
        return 0;
}

//DESTROY LIST
void DESTROY_LIST_OF_WORKERS(workers *list){
        worker_ptr temp1,temp2;
        temp1=(*list)->start;
        while(temp1!=NULL){
                temp2=temp1;
                temp1=temp2->next;
                free(temp2);
        }
        (*list)->start=NULL;
        free(*list);
        list=NULL;
        return;
}

void PRINT_LIST_OF_AVAILABLE_WORKERS(workers list){
        worker_ptr temp=list->start;
        fprintf(stderr,"-------------------\n");
        while(temp!=NULL){
                fprintf(stderr,"%d\n",temp->pid);
                fprintf(stderr,"%p\n",temp->next);
                temp=temp->next;
        }
        return;
}

//INSERT WORKER
//THE FIRST WORKER TO BE INSERTED HAS THE LOWEST PRIORITY TO GET A NEW JOB
void INSERT_WORKER_TO_WAITING_LIST(workers *list,pid_t pid){

        worker_ptr temp=(*list)->start;
        while(temp!=NULL){
                if(temp->pid==pid) return;
                temp=temp->next;
        }
        temp=NULL;
        char* check;
        temp=malloc(sizeof(worker_node));
        if(temp==NULL)  exit(21);

        temp->pid=pid;

        temp->next=(*list)->start;
        (*list)->start=temp;
        (*list)->number_of_available_workers++;
        //PRINT_LIST_OF_AVAILABLE_WORKERS(*list);
        return;
}

//EXTRACT WORKER
//THE FIRST WORKER WILL BE EXTRACTED ALWAYS AND WILL BE DELETED FROM AVAILABILITY LIST
int EXTRACT_WORKER_FROM_LIST(workers *list,pid_t *pid){

        worker_ptr temp=(*list)->start,temp1=NULL;
        if(temp==NULL) {fprintf(stderr,"NO AVAILABLE WORKER\n"); return 0;}
        *pid=temp->pid;
        temp1=temp;
        temp=temp1->next;
        free(temp1);
        if(temp==NULL) (*list)->start=NULL;
        //PRINT_LIST_OF_AVAILABLE_WORKERS(*list);
        return 1;
}


