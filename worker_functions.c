#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <poll.h>
#include <sys/wait.h>
#include "worker_functions.h"

volatile sig_atomic_t ok=1;

linfo LIST_CREAT(){
        linfo root=NULL;
        root=malloc(sizeof(info_node));
        if(root==NULL) exit(3);
        root->size=0;
        root->start=NULL;
        return root;
}

void DESTROY_LIST(linfo *root){
        node_ptr temp1,temp2;
        temp1=(*root)->start;
        while(temp1!=NULL){
                temp2=temp1;
                temp1=temp1->next;
                free(temp2);
        }
        (*root)->start=NULL;
        free(*root);
        root=NULL;
        return;
}

void PRINT_LIST(linfo list){
        node_ptr temp=list->start;
        fprintf(stderr,"-------------------\n");
        while(temp!=NULL){
                fprintf(stderr,"%d\n",temp->occurence);
                fprintf(stderr,"%s\n",temp->domain);
                fprintf(stderr,"%p\n",temp->next);
                temp=temp->next;
        }
        return;
}

//INSERT DOMAIN TO LIST IN CASE IT IS A NEW ONE

void INSERT_DOMAIN(linfo *root,char str[]){
        if(COMPARE_DATA(root,str)) return; //domain already exists
        node_ptr temp=NULL;
        char* check;
        temp=malloc(sizeof(node));
        if(temp==NULL)  exit(4);
        temp->occurence=1;
        check=strcpy(temp->domain,str);
        if(check==NULL) exit(5);
        temp->next=(*root)->start;
        (*root)->start=temp;
        (*root)->size++;
        //PRINT_LIST(*root);
        return;
}

//CHECK WETHER DOMAIN ALREADY EXISTS IN LIST OF DOMAINS
int COMPARE_DATA(linfo *root,char str[]){
        node_ptr temp;
        temp=(*root)->start;
        while(temp!=NULL){
                if(strcmp(str,temp->domain)==0){
                        temp->occurence++;
                        //fprintf(stderr,"occurence is  = %d\n",temp->occurence);
                        return 1;
                }
                temp=temp->next;
        }
        return 0;
}

//WRITE THE FOUND DOMAINS TO THE <filename>.out
void WRITE_DOMAINS_TO_FILE(linfo *root,int file){
        char buf[10];
        memset(buf,'\0',10);
        node_ptr temp=(*root)->start;
        while (temp!=NULL){
                write(file,temp->domain,strlen(temp->domain));
                write(file," ",1);
                sprintf(buf,"%d",temp->occurence);
                write(file,buf,strlen(buf));
                write(file,"\n",1);
                temp=temp->next;
        }
        return;
}

char* find_domain(char* ptr){ 
        char *p=NULL,*p1=NULL;
        //strstr to find the first dot "."
        p=strstr(ptr,".");
        p++;
        p1=strstr(p,".");
        if(p1==NULL)
                return p;
        p1++;
        return p1;   
}

//FUNCTION TO FIND ANY LINKS
void find_link(char *ptr,int file,int n,linfo *list){
        int i=0,j;
        char *buf=NULL,*p=NULL,*p1=NULL,*buffer=NULL;
        p=strstr(ptr,"http://");
        if(p==NULL) return;
        if(p!=NULL){
                p+=7;
                p1=strstr(p,"/");
                if(p1==NULL) return;
                p1+=2;
                buf=malloc(p1-p+1);
                memset(buf,'\0',p1-p+1);
                for(i=0;p!=p1;i++){
                        buf[i]=*p;
                        p++;
                }
                buf[i]='\0';
                for(j=0;j<i;j++) p--;
                
                if( strstr(buf,"http://")!= NULL){
                        p1-=8;
                        find_link(p1,file,n,list);//<------------------
                }
                else{
                        p1-=2;
                        buffer=malloc(p1-p+1);
                        memset(buffer,'\0',p1-p+1);
                        for(i=0; p!=p1; i++ ){
                                buffer[i]=*p;
                                p++;
                        }
                        buffer[i]='\0';
                        strcpy(buffer,find_domain(buffer));
                        INSERT_DOMAIN(list,buffer);
                        free(buffer);
                        buffer=NULL;
                        free(buf);
                        buf=NULL;
                        find_link(p1,file,n,list);//<---------------------
                }
        }       
}


//signal handler when SIGINT is received
void sig_kill(int n){
        fprintf(stderr,"EXITING PROCCESS %d\n",getpid());
        ok=0;
        fflush(stdout);
}

//function WORKER is the body of the worker-process
void WORKER(int pipe_descriptor){

        signal(SIGINT,sig_kill);        //set signal handler

        int size,i=0,source,result,length,ret=0;
        char buffer[8192],*file=NULL,*file_to_write=NULL;

        linfo list=NULL;
        list=LIST_CREAT();
        //worker is kept alive with variavle ok
        while(ok){
                if(ok==0) break;
                //fprintf(stderr,"IN WORKER\n");
                //fprintf(stderr,"pipe descriptor = %d\n",pipe_descriptor);
                //fprintf(stderr,"%d\n",pipe_descriptor);
        
			
                size=read(pipe_descriptor,&length,sizeof(length));
                if(size==-1 && ok==1) size=read(pipe_descriptor,&length,sizeof(length));
                if(ok==0) break;
                perror("read1");
                fprintf(stderr,"i read %d bytes\n",size);
                fprintf(stderr,"size of string to be read = %d\n",length);
  
                file=malloc(length+1);
                memset(file,'\0',length+1);
                size=read(pipe_descriptor,file,length);
                if(size==-1 && ok==1) size=read(pipe_descriptor,file,length);
                perror("read2");
                fprintf(stderr,"i read %d bytes\n",size);
                fprintf(stderr,"file to open is %s\n",file);
                if(ok==0) break;
                source=open(file,O_RDWR,0777);
                perror("file");
                //if(source==-1) exit(-1);
        
                file_to_write=malloc(length+5);
                memset(file_to_write,'\0',length+5);
                strcpy(file_to_write,file);
                strcat(file_to_write,".out");
                fprintf(stderr,"%s\n",file_to_write);
                result=open(file_to_write,O_CREAT|O_RDWR,0777);
                perror("file");
                //if(result==-1) exit(-1);
                if(ok==0) break;

                while(size=read(source,buffer,8192)>0 && ok==1){
                        if(ok==0) break;
                        find_link(buffer,result,0,&list);
                }   
                if(ok==0) break;    
                //PRINT_LIST(list);
                WRITE_DOMAINS_TO_FILE(&list,result);
                DESTROY_LIST(&list);
                close(source);
                close(result);
                raise(SIGSTOP); //child stops itself
                perror("signal");
                length=0;
                free(file);
                file=NULL;
                free(file_to_write);
                file_to_write=NULL;
        }
        //clean up everything
        DESTROY_LIST(&list);
        close(source);
        close(result);
        free(file);
        file=NULL;
        free(file_to_write);
        file_to_write=NULL;
        raise(SIGTERM);  //child kill itself
}

