#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <poll.h>
#include <sys/wait.h>
#include "sniffer_functions.h"

#define READ  0
#define WRITE 1
#define PATH "named-pipe"

//global variables that will definitely be compiled and executed atomically
volatile sig_atomic_t flag=1;           
volatile sig_atomic_t parent_id=0;
volatile sig_atomic_t suspended=0;

//signal handler for signal SIGINT
void exit_all(int signo){
        fprintf(stderr,"\nQUITTING ALL PROCESSES\n");
        flag=0;         //set flag to 0 to end parent process
        fflush(stdout);
}

//signal handler for signal SIGCHLD
void child(int sig_no){
        int status;
        //get the process id of child that changed its state
        suspended=waitpid(-1,&status,WUNTRACED);
        //checking status of child that changed state
        if(WIFEXITED(status)) { fprintf(stderr,"exited\n"); /*suspended=0;*/ }
        if(WEXITSTATUS(status)) { fprintf(stderr,"exited\n"); /*suspended=0;*/ }
        if(WIFSIGNALED(status)) fprintf(stderr,"signaled\n");
        if(WTERMSIG(status)) { fprintf(stderr,"terminated\n"); /*suspended=0;*/ }
        if(WCOREDUMP(status)) { fprintf(stderr,"core dump\n"); suspended=0; }
        if(WIFSTOPPED(status)) fprintf(stderr,"stopped\n");  
        if(WSTOPSIG(status)) fprintf(stderr,"stopsig\n");
        if(WIFCONTINUED(status)) { fprintf(stderr,"continued\n"); suspended=0; }
        fprintf(stderr,"child suspended =  %d\n",suspended);
}

//MAIN FUNCTION
int main(int argc,char *argv[]){

        static struct sigaction act1,act2;

        act1.sa_handler=exit_all;               //set signal handler
        //act1.sa_flags=SA_RESTART;             
        sigfillset(&(act1.sa_mask));            //all signals will be ignored 
        sigdelset(&(act1.sa_mask),SIGINT);      //except for signal SIGINT
        //sigdelset(&(act1.sa_mask),SIGCHLD);
        sigdelset(&(act1.sa_mask),SIGSEGV);     //and SIGSEGV (for segmentation faults!)
        //sigdelset(&(act1.sa_mask),SIGSTOP);
        sigaction(SIGINT,&act1,NULL);         
        
        act2.sa_handler=child;                  //the same as above
        //act2.sa_flags=SA_RESTART;
        sigfillset(&(act2.sa_mask));
        //sigdelset(&(act2.sa_mask),SIGINT);
        sigdelset(&(act2.sa_mask),SIGCHLD);
        sigdelset(&(act2.sa_mask),SIGSEGV);
        //sigdelset(&(act2.sa_mask),SIGSTOP);
        sigaction(SIGCHLD,&act2,NULL);

        int p[2],size=0,file,i,j=0,length=0,named_pipe,ret=0;
        char buffer[50],*file_to_send=NULL,*location="../.";
        pid_t pid,worker[100],process;

        memset(buffer,'\0',50);

        workers list=NULL;                      //creating list for available to hold available workers
        list=CREATE_LIST_OF_AVAILABLE_WORKERS();
        if(argc==2){                            //if path is given from command line for inotifywait to monitor
                location=malloc(strlen(argv[1])+1);
                memset(location,'\0',strlen(argv[1])+1);
                strcpy(location,argv[1]);
                fprintf(stderr,"location to monitor %s\n",location);
        }
        else fprintf(stderr,"location to monitor %s\n",location);

        //path to find executable           //arguments to set behaviour of inotifywait
        char path[]="/usr/bin/inotifywait", *command[]={"inotifywait","-m","-e","create","--format","%f",location,NULL};
        
        //requesting named pipe for comunication between parent and child processes
        //only one named pipe is used for this task
        if(mkfifo(PATH,0666)==-1) {perror("mkfifo\n"); exit(-10);}
        named_pipe=open(PATH,O_RDWR);
        if(named_pipe==-1) {perror("open named pipe\n"); exit(-10);}        
   
        //requesting pipe for comunication between parent process and inotifywait
        if(pipe(p)==-1) {perror("pipe : "); exit(-1);}
        switch (pid=fork()){                    //creating of child process
        case -1: {perror("fork :"); exit(-1);}
        case 0:  {      //setting file descriptors before calling exec to run inotifywait
                        close(p[READ]);
                        dup2(p[WRITE],1); //the end for writting goes to the stdout of child process
                        close(p[WRITE]);
                        execvp(path,command);
                        perror("execvp");
                }
        default:{       //flag is responsible to keep parent process working
                        while(flag){

                                if(flag==0) break; //flag is set to 0 only if user sends SIGINT(ctrl+C) from keyboard
                                memset(buffer,'\0',50);
                                i=0;
                                while(i<50){
                                        size=read(p[READ],&(buffer[i]),1); //parent holds until a file is created
                                        if(flag==0) break;
                                        //manually restart reading if read failed due to receival of a signal
                                        if(size==-1 && suspended!=0) continue;
                                        if(buffer[i]=='\n') break;
                                        i++;
                                }
                                if(flag==0) break;
                                buffer[i]='\0';
                                length=strlen(buffer);

				if(flag==0) break;
                                if(suspended!=0){
                                        for(i=0;i<10;i++){
                                                if(worker[i]==suspended){
                                                        //suspended worker is inserted to list
                                                        INSERT_WORKER_TO_WAITING_LIST(&list,worker[i]);
                                                }
                                        } 
                                }
                                if(flag==0) break;
                                
                                if(EMPTY_LIST(list)){   //if no worker has been yet suspended
                                        switch(worker[j]=fork()){
                                        case -1:{perror("fork for worker :"); exit(-1);}   
                                        case 0 :{
                                                WORKER(named_pipe); //if in child process call the worker function
                                                }  
                                        default:{   
                                                //if in parent process
                                                //inform worker of how many bytes to read
                                                write(named_pipe,&length,sizeof(length)); 
                                                //inform worker which file to work with                                       
                                                write(named_pipe,buffer,length);
                                                j++;
                                                }                
                                        }
                                }
                                else{
                                        //pending workers exist
                                        EXTRACT_WORKER_FROM_LIST(&list,&process);
                                        size=write(named_pipe,&length,sizeof(length));
                                        size=write(named_pipe,buffer,length);
                                        //send signal SIGCONT to child with pid worker
                                        ret=kill(process,SIGCONT);
                                        perror("signal SIGCONT");
                                }
                                if(flag==0) break;
                        }
                }
                //if signal SIGINT is received parent process cleans up everything
                close(named_pipe);
                unlink(PATH);
                close(p[READ]);
                //send signal via kill to kill "inotifywait" and all remaining child processes
                kill(-parent_id,SIGKILL);
        }
        exit(0);
}
