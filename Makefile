CC=gcc

CFLAGS=-c -w

all: sniffer
	./sniffer

sniffer: sniffer.o sniffer_functions.o worker_functions.o
	$(CC) sniffer.o sniffer_functions.o worker_functions.o -o sniffer 

sniffer.o: 
	$(CC) $(CFLAGS) sniffer.c

sniffer_functions.o: 
	$(CC) $(CFLAGS) sniffer_functions.c

worker_functions.o: 
	$(CC) $(CFLAGS) worker_functions.c

clean:
	rm -rf *o sniffer
