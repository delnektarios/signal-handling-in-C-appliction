
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#ifndef FUNCTIONS //include guard

typedef struct node *node_ptr;
typedef struct info_node *linfo;

typedef struct info_node{
        int size;
        node_ptr start;
}info_node;

typedef struct node{
        int occurence;
        char domain[20];
        node_ptr next;
}node;

linfo LIST_CREAT();

void DESTROY_LIST(linfo *root);

void PRINT_LIST(linfo list);

void INSERT_DOMAIN(linfo *root,char str[]);

int COMPARE_DATA(linfo *root,char str[]);

void WRITE_DOMAINS_TO_FILE(linfo *root,int file);

char* find_domain(char* ptr);

void find_link(char *ptr,int file,int n,linfo *list);

void WORKER(int pipe_descriptor);


#endif








