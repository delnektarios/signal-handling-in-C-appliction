
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#ifndef FUNCTIONS //include guard

typedef struct worker_node *worker_ptr;
typedef struct workers_info *workers;

typedef struct workers_info{
        int number_of_available_workers;
        worker_ptr start;
}workers_info;

typedef struct worker_node{
        pid_t pid;
        worker_ptr next;
}worker_node;

workers CREATE_LIST_OF_AVAILABLE_WORKERS();

int EMPTY_LIST(workers list);

void DESTROY_LIST_OF_WORKERS(workers *list);

void PRINT_LIST_OF_AVAILABLE_WORKERS(workers list);

void INSERT_WORKER_TO_WAITING_LIST(workers *list,pid_t pid);

int EXTRACT_WORKER_FROM_LIST(workers *list,pid_t *pid);



#endif
